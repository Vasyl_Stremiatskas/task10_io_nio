package com.stremiatskas.Menu;

import com.stremiatskas.IO.*;
import com.stremiatskas.NIO.BuffClass;

import com.stremiatskas.Serialization.SerializedObject;


import java.io.File;
import java.io.IOException;
import java.util.*;

public class ViewMenu {

    private Map<String, String> menu;
    private Map<String, Print> taskMenu;
    private static Scanner input = new Scanner(System.in);


    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", new String("1 - FileInputStream read"));
        menu.put("2", new String("2 - BufferReader"));
        menu.put("3", new String("3-PushInputStream"));
        menu.put("4", new String("4 - Source code read"));
        menu.put("5", new String("5 - Directory"));
        menu.put("6", new String("6 - Serialization/Deserialization"));

        menu.put("Exit", new String("Exit"));
    }

    public ViewMenu() {
        setMenu();
        taskMenu = new LinkedHashMap<>();
        taskMenu.put("1", this:: usealRead_1);
        taskMenu.put("2", this::buffRead_2);
        taskMenu.put("3", this::implIS_3);
        taskMenu.put("4", this::sourceCode_4);
        taskMenu.put("5", this::directory_5);
        taskMenu.put("6", this:: droidShip);


    }

    private void usealRead_1() throws IOException {
    UsealReader usealReader = new UsealReader();
    usealReader.readFin();
    }

    private void buffRead_2() throws IOException {
        BuffReader buffReader = new BuffReader();
        buffReader.readBuff();
    }

    private void implIS_3() throws IOException {
        ImplInputStream implInputStream = new ImplInputStream();
        implInputStream.implIS();
    }

    private void sourceCode_4() throws IOException {
        ReadSourceCode readSourceCode = new ReadSourceCode();
        readSourceCode.readSC();
    }

    private void directory_5() throws IOException {
        File file = new File("D:\\Helen!\\теорія кольору");
        DisplayContentOfDirectory displayContentOfDirectory = new DisplayContentOfDirectory();
        displayContentOfDirectory.displayDirectory(file);
    }

    private void nioBuff_6() throws IOException {
        BuffClass buffClass = new BuffClass();
        buffClass.buffChanel();
    }

    private void droidShip() throws IOException, ClassNotFoundException {
        SerializedObject serializedO = new SerializedObject();
        serializedO.serializedObject();
        serializedO.deserializedObject();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Select task, that you'd like check");
            keyMenu = input.nextLine().toUpperCase();
            try {
                taskMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Exit"));
    }
}