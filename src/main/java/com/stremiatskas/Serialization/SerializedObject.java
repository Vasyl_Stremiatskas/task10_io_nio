package com.stremiatskas.Serialization;

import java.io.*;

public class SerializedObject {


    String fileN = "D:\\Helen!\\Java\\ser.txt";
    public void serializedObject() throws IOException {
        DroidShip object = new DroidShip(25);



        try {
            File file = new File(fileN);
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));

            out.writeObject(object);

                System.out.println("Object serialized");

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void deserializedObject() throws IOException, ClassNotFoundException {
        File file = new File(fileN);
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
        DroidShip objectOut = (DroidShip) in.readObject();
        System.out.println("Mission -  " + objectOut.mission);
        System.out.println("Cap name - " + objectOut.capName );
        System.out.println("Count of droids: " + objectOut.getDroids());
    }
}