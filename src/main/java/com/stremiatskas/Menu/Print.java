package com.stremiatskas.Menu;

import java.io.IOException;
import java.sql.SQLException;

public interface Print {
    public void print() throws SQLException, IOException, InterruptedException, ClassNotFoundException;
}